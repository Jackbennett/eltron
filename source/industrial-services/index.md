---
title: Industrial Services
slug: industrial
date: 2018-01-06 18:09:13
---

Existing equipment can be modified and updated for maximum efficiency and trouble free running. Past projects have required such conversion or retrofitting of machinery utilizing modern PLC control.

![Industrial Boiler Control Panel](pressure-cooker-control.jpg)

Our service offers on site diagnosis and repair to static equipment on production machinery that have included the following industries;

* Cosmetic, bottling &amp; labeling
* Food, pressure cookers, chillers, ice cream
* Injection molding
* Printing, on paper and polythene
* Paper converters
* Hospitals

New build projects involve meeting our clients' specifications such as;

* Design and construct control panels for pressure cooker automation
* Repetitive testing for in-house calibration
* Automate sequential processing
* Bespoke test rigs for rapid process testing
