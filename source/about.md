---
title: About
slug: about
description: Who are we and what we do.
date: 2017-12-23 19:15:47
---

Eltron Electronic Service has been trading successfully since 1983 and is dedicated to providing an excellent service and replacement parts at competitive prices.

![Section of a custom build pressure cooker control unit](board-1.jpg)

* Experienced in Industry working on a variety of machinery and equipment
* Experienced in Maritime electronic controls
* Refurbishment of existing control systems on machinery with more efficient and cost effective equipment
* Panel Design and programme of PLC's (Mitsubishi &amp; Panasonic, formerly Matsushita) to your specific requirements

Our facilities and test equipment allow Eltron to carry out extensive fault finding and repair work to our customers' electronic and electrical equipment to component level, often without the aid of circuit diagrams. Manufacturers have come and gone or support has expired, leaving companies with obsolete equipment that can be repaired or refurbished before expensive alternatives need to be purchased.

Circuit board repairs can often cost less than a replacement and we endeavor to offer a fast turn-a-round on electronic problems to ensure that your equipment is back in use as quickly as possible.

> As a skilled engineer, standards are set high and quality is paramount

![Custom fitted control panels](panels-1.jpg)
