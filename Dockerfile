FROM node:18-alpine

WORKDIR /app
ENV NODE_PATH=/node_modules
COPY package*.json ./
RUN npm update npm && npm install --no-save

COPY . .
RUN npm run build

EXPOSE 4000

CMD [ "npx", "hexo", "server" ]
