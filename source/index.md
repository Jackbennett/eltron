---
title: Site
slug: site
description: What we did.
layout: splash
date: 2022-12-15 23:00:00
---

Eltron Electronic Service has been trading successfully for 39 years, 1983 until 2022 were many satisfied customers found excellent service and replacement parts.

Tony &amp; Elaine wish their former employees &amp; customer success in the future.

<div style="text-align: center;">

![Diagnosing generator faults](repairs/diag_gen_faults.jpg)
![Custom fitted control panels](panels-1.jpg)
![Club Med 2 Vessel](marine-services/club_med_2.jpg)
![Section of a custom build pressure cooker control unit](board-1.jpg)

</div>
