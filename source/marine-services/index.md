---
title: Marine Services
slug: marine
date: 2018-01-06 18:20:41
---

Our fully equipped workshop with a pair of Cyclic Converters each rated at 3kW makes them ideal for testing shipboard equipment ashore;

* Can simulate actual presence of the alternators fitted on board your vessel
* This means we are able to truly operate equipment for repair at the correct frequency (60Hz) and voltage
* Perfect for testing switchboard components such as; voltage regulators, paralleling devices etc
* Frequency conscience components can be proof tested
* When used as a pair, the units cross connections can be made for testing syncroscopes and paralleling devices
* Essential in the quality control of equipment prior to return to vessel

![Club Med 2 Ship](club_med_2.jpg)

Specialist marine electronic repairs can involve traveling with the vessel to or from foreign destinations and engineers from Eltron Electronic Service have attended vessels around the world.

Some of the vessels to have benefited from our expertise include; M.Y. Windsurf, M.Y. Windspirit, Island Escape, M.V. Explorer, Grandeur of the Seas, Vision of the Seas, Club Med 2. M.V. Zenith.

## Marine Repair and Service

* Automatic voltage system
* Paralleling devices
* Switchboard instruments
* Alarm systems
* Monitoring systems
* Process controlling systems
* Fuel control boards
* Fuel blending systems
* Temperature controls
* Voltage current converters
* Scaling devices
* Fire alarm control circuit boards
* Load cells and amplifiers
* Tank gauges and amplifiers
* Pressure sensing
* Anemometer systems

> Specialist test equipment: 3kW rated Cyclic Converters

Bespoke projects have involved working with our clients to develop, design and build;

* Fire door monitoring control system
* Battery condition monitoring system for backup batteries failing within critical systems
* Alternator automatic voltage control

![3 Phase Marine power](3phase_gens.jpg)
