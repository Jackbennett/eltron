---
title: Repairs
slug: repairs
date: 2018-01-06 17:16:08
---

We will collect and deliver items within the local area. Alternatively items for repair can be sent directly to the workshop. A quotation will be given before commencement of work if required. All repairs carry a 3 month warranty.

> Fair competitive pricing – not set pricing

![Diagnosing generator faults](diag_gen_faults.jpg)

* Repair and re-manufacture all types of printed circuit boards
* Physically damaged circuit boards and assemblies to component level [track repairs]
* Experience in all areas of solid state controls
* Analogue and digital printed circuit boards
* I/O interface boards
* PLC's – repairs and programming
* Power supplies – linear and switching
* UPS systems
* SCR assemblies
* Clutch and brake controls
* Temperature controller (including calibration)
* Electronic counters and timers
* Process control systems
* Instrument calibration
* Encoders
* Plant room controls
* Custom, O.E.M. and depreciated controls

We can also modify and update existing equipment for maximum efficiency and trouble free running. Projects have included retrofitting machinery utilizing modern PLC control.
